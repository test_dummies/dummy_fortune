package test_dummies.fortune;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

//Imports for the Volley Singleton

//Imports for JSON Reading
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Random;

//Imports for data storing
import java.io.IOException;
import java.io.InputStream;


public class FortuneActivity extends AppCompatActivity {

    //Used for button animation of Give Fortune button
    ImageButton buttonImage;
    ImageButton buttonDown;

    //Used for button animation of Check Horoscope button
    ImageButton buttonImage2;
    ImageButton buttonDown2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fortune);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //This section is used to make the animation for the gradient background
        LinearLayout your_Layout = (LinearLayout) findViewById(R.id.fortune_container);
        AnimationDrawable animationDrawable = (AnimationDrawable) your_Layout.getBackground();
        animationDrawable.setEnterFadeDuration(4000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();

        final JSONArray fortunes = pull_json();
        final Handler handler = new Handler(); //Needed to make delay

        //This allows a mp3 sound to be used whenever called
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.click);

        //This section creates the button and does its animation with a delay handler
        buttonImage = (ImageButton) findViewById(R.id.imageButton);
        buttonDown = (ImageButton) findViewById(R.id.buttonDown1);
        buttonImage.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonImage.setVisibility(View.INVISIBLE);
                buttonDown.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonDown.setVisibility(View.INVISIBLE);
                        buttonImage.setVisibility(View.VISIBLE);
                        randomFortune(fortunes);
                    }
                }, 125);
            }
        });

        buttonImage2 = (ImageButton) findViewById(R.id.imageButton2);
        buttonDown2 = (ImageButton) findViewById(R.id.buttonDown2);
        buttonImage2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonImage2.setVisibility(View.INVISIBLE);
                buttonDown2.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonDown2.setVisibility(View.INVISIBLE);
                        buttonImage2.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(getApplicationContext(), Horoscope.class);
                        startActivity(intent);
                    }
                }, 125);
            }
        });

    }

    protected JSONArray pull_json(){
        String json;
        JSONArray fortuneArray = null;
        try{
            InputStream is = getAssets().open("fortune.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer,"UTF-8");
            fortuneArray = new JSONArray(json);

        }
        catch (IOException e) {
            e.printStackTrace();
        }catch (JSONException j){
            j.printStackTrace();
        }
        return fortuneArray;
    }

    protected void randomFortune(JSONArray fortun){
        Random randNum = new Random();
        JSONObject fortuObj = null;
        String foruStr = "";
        try {
            fortuObj = fortun.getJSONObject(randNum.nextInt(100));
            foruStr = fortuObj.getString("fortune");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Toast toast = Toast.makeText(FortuneActivity.this, "" + foruStr, Toast.LENGTH_LONG);

        ViewGroup group = (ViewGroup) toast.getView();
        TextView messageTextView = (TextView) group.getChildAt(0);
        messageTextView.setTextSize(25);
        toast.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_fortune, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //The intent is used to travel into another page
            Intent intent = new Intent(this, Settings.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
