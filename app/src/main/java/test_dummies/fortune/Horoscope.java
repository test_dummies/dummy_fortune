package test_dummies.fortune;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Horoscope extends AppCompatActivity {

    //Used for button animation of Give Fortune button
    ImageButton buttonAries;
    ImageButton buttonAriesDown;

    ImageButton buttonAquarius;
    ImageButton buttonAquariusDown;

    ImageButton buttonLibra;
    ImageButton buttonLibraDown;

    ImageButton buttonLeo;
    ImageButton buttonLeoDown;

    ImageButton buttonCapricorn;
    ImageButton buttonCapricornDown;

    ImageButton buttonCancer;
    ImageButton buttonCancerDown;

    ImageButton buttonTaurus;
    ImageButton buttonTaurusDown;

    ImageButton buttonGemini;
    ImageButton buttonGeminiDown;

    ImageButton buttonVirgo;
    ImageButton buttonvirgoDown;

    ImageButton buttonScorpio;
    ImageButton buttonScorpioDown;

    ImageButton buttonSagittarius;
    ImageButton buttonSagittariusDown;

    ImageButton buttonPisces;
    ImageButton buttonPiscesDown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horoscope);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //This section is used to make the animation for the gradient background
        LinearLayout your_Layout = (LinearLayout) findViewById(R.id.horo_container);
        AnimationDrawable animationDrawable = (AnimationDrawable) your_Layout.getBackground();
        animationDrawable.setEnterFadeDuration(4000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();

        //This allows a mp3 sound to be used whenever called
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.click);
        final Handler handler = new Handler(); //Needed to make delay

        buttonAries = (ImageButton) findViewById(R.id.imageButtonAries);
        buttonAriesDown = (ImageButton) findViewById(R.id.buttonDownAries);
        buttonAries.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonAries.setVisibility(View.INVISIBLE);
                buttonAriesDown.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonAriesDown.setVisibility(View.INVISIBLE);
                        buttonAries.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(getApplicationContext(), Aries.class);
                        startActivity(intent);
                    }
                }, 125);
            }
        });

        buttonAquarius = (ImageButton) findViewById(R.id.imageButtonAquarius);
        buttonAquariusDown = (ImageButton) findViewById(R.id.buttonDownAquarius);
        buttonAquarius.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonAquarius.setVisibility(View.INVISIBLE);
                buttonAquariusDown.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonAquariusDown.setVisibility(View.INVISIBLE);
                        buttonAquarius.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(getApplicationContext(), Aquarius.class);
                        startActivity(intent);
                    }
                }, 125);
            }
        });

        buttonCancer = (ImageButton) findViewById(R.id.imageButtonCancer);
        buttonCancerDown = (ImageButton) findViewById(R.id.buttonDownCancer);
        buttonCancer.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonCancer.setVisibility(View.INVISIBLE);
                buttonCancerDown.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonCancerDown.setVisibility(View.INVISIBLE);
                        buttonCancer.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(getApplicationContext(), Cancer.class);
                        startActivity(intent);
                    }
                }, 125);
            }
        });

        buttonCapricorn = (ImageButton) findViewById(R.id.imageButtonCapricorn);
        buttonCapricornDown = (ImageButton) findViewById(R.id.buttonDownCapricorn);
        buttonCapricorn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonCapricorn.setVisibility(View.INVISIBLE);
                buttonCapricornDown.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonCapricornDown.setVisibility(View.INVISIBLE);
                        buttonCapricorn.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(getApplicationContext(), Capricorn.class);
                        startActivity(intent);
                    }
                }, 125);
            }
        });

        buttonLeo = (ImageButton) findViewById(R.id.imageButtonLeo);
        buttonLeoDown = (ImageButton) findViewById(R.id.buttonDownLeo);
        buttonLeo.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonLeo.setVisibility(View.INVISIBLE);
                buttonLeoDown.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonLeoDown.setVisibility(View.INVISIBLE);
                        buttonLeo.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(getApplicationContext(), Leo.class);
                        startActivity(intent);
                    }
                }, 125);
            }
        });

        buttonLibra = (ImageButton) findViewById(R.id.imageButtonLibra);
        buttonLibraDown = (ImageButton) findViewById(R.id.buttonDownLibra);
        buttonLibra.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonLibra.setVisibility(View.INVISIBLE);
                buttonLibraDown.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonLibraDown.setVisibility(View.INVISIBLE);
                        buttonLibra.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(getApplicationContext(), Libra.class);
                        startActivity(intent);
                    }
                }, 125);
            }
        });

        buttonGemini = (ImageButton) findViewById(R.id.imageButtonGemini);
        buttonGeminiDown = (ImageButton) findViewById(R.id.buttonDownGemini);
        buttonGemini.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonGemini.setVisibility(View.INVISIBLE);
                buttonGeminiDown.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonGeminiDown.setVisibility(View.INVISIBLE);
                        buttonGemini.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(getApplicationContext(), Gemini.class);
                        startActivity(intent);
                    }
                }, 125);
            }
        });

        buttonPisces = (ImageButton) findViewById(R.id.imageButtonPisces);
        buttonPiscesDown = (ImageButton) findViewById(R.id.buttonDownPisces);
        buttonPisces.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonPisces.setVisibility(View.INVISIBLE);
                buttonPiscesDown.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonPiscesDown.setVisibility(View.INVISIBLE);
                        buttonPisces.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(getApplicationContext(), Pisces.class);
                        startActivity(intent);
                    }
                }, 125);
            }
        });

        buttonVirgo = (ImageButton) findViewById(R.id.imageButtonVirgo);
        buttonvirgoDown = (ImageButton) findViewById(R.id.buttonDownVirgo);
        buttonVirgo.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonVirgo.setVisibility(View.INVISIBLE);
                buttonvirgoDown.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonvirgoDown.setVisibility(View.INVISIBLE);
                        buttonVirgo.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(getApplicationContext(), Virgo.class);
                        startActivity(intent);
                    }
                }, 125);
            }
        });

        buttonTaurus = (ImageButton) findViewById(R.id.imageButtonTaurus);
        buttonTaurusDown = (ImageButton) findViewById(R.id.buttonDownTaurus);
        buttonTaurus.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonTaurus.setVisibility(View.INVISIBLE);
                buttonTaurusDown.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonTaurusDown.setVisibility(View.INVISIBLE);
                        buttonTaurus.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(getApplicationContext(), Taurus.class);
                        startActivity(intent);
                    }
                }, 125);
            }
        });

        buttonScorpio = (ImageButton) findViewById(R.id.imageButtonScorpio);
        buttonScorpioDown = (ImageButton) findViewById(R.id.buttonDownScorpio);
        buttonScorpio.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonScorpio.setVisibility(View.INVISIBLE);
                buttonScorpioDown.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonScorpioDown.setVisibility(View.INVISIBLE);
                        buttonScorpio.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(getApplicationContext(), Scorpio.class);
                        startActivity(intent);
                    }
                }, 125);
            }
        });

        buttonSagittarius = (ImageButton) findViewById(R.id.imageButtonSagittarius);
        buttonSagittariusDown = (ImageButton) findViewById(R.id.buttonDownSagittarius);
        buttonSagittarius.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                mp.start();
                buttonSagittarius.setVisibility(View.INVISIBLE);
                buttonSagittariusDown.setVisibility(View.VISIBLE);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        buttonSagittariusDown.setVisibility(View.INVISIBLE);
                        buttonSagittarius.setVisibility(View.VISIBLE);
                        Intent intent = new Intent(getApplicationContext(), Sagittarius.class);
                        startActivity(intent);
                    }
                }, 125);
            }
        });
    }
}
