package test_dummies.fortune;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;



class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ViewHolder> {

    interface ClickListener {
        //void onClick(Post d);
    }
    private ClickListener mClickListener;
    ClickListener getClickListener() {return mClickListener;}
    void setClickListener(ClickListener c) { mClickListener = c;}

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mPost;

        ViewHolder(View itemView) {
            super(itemView);
            this.mPost = (TextView) itemView.findViewById(R.id.listItemIndex);
        }
    }

    private ArrayList<String> mData;
    private LayoutInflater mLayoutInflater;

    ItemListAdapter(Context context, ArrayList<String> data) {
        mData = data;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_item, null);
        return new ViewHolder(view);
    }

    @Override
    //Helps put attributes onto the views
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String d = mData.get(position);
        if(position == 0) {

            holder.mPost.setText( d + "\n");
        }
    }
}