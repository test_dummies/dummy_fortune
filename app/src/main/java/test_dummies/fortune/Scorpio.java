package test_dummies.fortune;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Scorpio extends AppCompatActivity {
    ArrayList<String> mData = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scorpio);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //This section is used to make the animation for the gradient background
        LinearLayout your_Layout = (LinearLayout) findViewById(R.id.horo_container);
        AnimationDrawable animationDrawable = (AnimationDrawable) your_Layout.getBackground();
        animationDrawable.setEnterFadeDuration(4000);
        animationDrawable.setExitFadeDuration(4000);
        animationDrawable.start();

        // Instantiate the RequestQueue.
        RequestQueue queue = VolleySingleton.getInstance(this).getRequestQueue();
        String[] signs= {"aries","taurus","gemini","cancer","leo","virgo","libra",
                "scorpio","sagittarius","capricorn","aquarius","pisces"};
        String url = "http://horoscope-api.herokuapp.com/horoscope/today/scorpio";
        StringRequest stringRequest = null;

        // Request a string response from the provided URL.
        //TODO Set up get route for data
        //for(int i = 0; i<signs.length; i++) {
        //String fullUrl = url+signs[i];
        stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        populateListFromVolley(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("aoj219", "That didn't work!");
            }
        });
        queue.add(stringRequest);
        //}

    }

    //Populate the data from JSON
    protected void populateListFromVolley(String response){
        String str = "";

        //Loads in the data from the JSON
        try {
            JSONObject json= new JSONObject(response);
            str = json.getString("horoscope");
            mData.add(str);
            CreateView();

        } catch (final JSONException e) {
            Log.d("aoj219", "Error parsing JSON file: " + e.getMessage());
            return;
        }
        Log.d("aoj219", "Successfully parsed JSON file.");

    }

    protected void CreateView(){
        //Sets up list view on the app
        RecyclerView rv = (RecyclerView) findViewById(R.id.horolist_view);
        rv.setLayoutManager(new LinearLayoutManager(this));
        final ItemListAdapter adapter = new ItemListAdapter(this, mData);
        rv.setAdapter(adapter);
    }
}
