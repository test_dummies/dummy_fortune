package test_dummies.fortune;

import com.android.volley.RequestQueue;
import android.content.Context;
import com.android.volley.toolbox.Volley;

/*Class that helps to create Volley Singleton Pattern to */
public class VolleySingleton {
    private static VolleySingleton sInstance;
    private RequestQueue sRequestQueue;

    private VolleySingleton (Context context) {
        sRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    public static synchronized VolleySingleton getInstance(Context context){
        if (sInstance==null){
            sInstance = new VolleySingleton(context);
        }
        return sInstance;
    }

    public RequestQueue getRequestQueue(){
        return sRequestQueue;
    }
}
